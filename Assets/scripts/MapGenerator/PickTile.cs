﻿using UnityEngine;
using System.Collections.Generic;

public class PickTile : MonoBehaviour {
	public List<Sprite> sprites=new List<Sprite>();

	// Use this for initialization
	void Start () {
		int rnd=(int)(Random.value*sprites.Count);
		GetComponent<SpriteRenderer>().sprite=sprites[rnd];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
