﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class mapgenerator : MonoBehaviour {
    
    public Vector2 CellSize = new Vector2(1, 1);
    public int MapsizeX;
    public int MapsizeY;
    public float Gritgab = 0.01f;
    public List<GameObject> MapCells;
    public GameObject Map;
    public bool BuildByPlay = false;

    public GameObject NpcPrefab;
    public int maxGreenNpc;
    public int maxBlueNpc;
    public int maxRedNpc;

    int npcSumme;
    public  List<Transform> Openposition =new List<Transform>();
    public GameObject Wallprefab;
    public GameObject Goalprefab;
    void Awake()
    {
        if(BuildByPlay)
            MapGenerator(MapsizeX, MapsizeY);
    }
    //"blubb" suche schreiber/in dieses Kommentars! Wer zur Hölle macht Kommentare genau wie ich?
  public void Respawn()
    {
        Openposition.Clear();
        int _GreenNpc = maxGreenNpc;
        int _BlueNpc= maxBlueNpc;
        int _redNpc =maxRedNpc;
        npcSumme = maxGreenNpc + maxBlueNpc + maxRedNpc;
        foreach(MapCell bigcell in Map.GetComponent<MapInformation>().MapCells )
        {
           
            bigcell.GetComponent<MapCell>().Freeposition();
          
        }
        foreach (MapCell bigcell in Map.GetComponent<MapInformation>().MapCells)
        {
            foreach (Transform smallCell in bigcell.freeCells)
            {
                
                Openposition.Add(smallCell);
            }
        }
      

        if (npcSumme > Openposition.Count)
        {
            Debug.LogWarning("Es sind zu wenige freie position für einen Npc! wennige als verlangt werden erscheinen.");
            int i = npcSumme;
            do
            {
                int rand = Random.Range(0, Openposition.Count - 1);
                GameObject Npc = (GameObject)Instantiate(NpcPrefab, Openposition[rand].position, Openposition[rand].rotation);
               // Openposition.Remove(Openposition[rand]);
                int r = Random.Range(1, 3);
                
                switch (r)
                {
                    case 1:
                        if (_GreenNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        break;

                    case 2:
                        if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        break;

                    case 3:
                        if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        break;
                }
                i--;
            } while (i >0);
        }
        else if(npcSumme == Openposition.Count)
        {
            int i = Openposition.Count - 1;
            Debug.LogWarning("es sind gerade genügend freie Positionen vorhanden.");
            do
            {
                int rand = Random.Range(0, Openposition.Count - 1);
                GameObject Npc = (GameObject)Instantiate(NpcPrefab, Openposition[rand].position, Openposition[rand].rotation);
                //Openposition.Remove(Openposition[rand]);
                i--;
                int r = Random.Range(1, 3);
              
                switch (r)
                {
                    case 1:
                        if (_GreenNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        break;

                    case 2:
                        if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        break;

                    case 3:
                        if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        break;
                }
            } while (i>0);
        }
        else
        {
            int i =npcSumme;
            Debug.LogWarning("es sind  genügend freie Positionen vorhanden.");
           do
            {
                int rand = Random.Range(0, Openposition.Count - 1);
                
                GameObject Npc = (GameObject)Instantiate(NpcPrefab, Openposition[rand].position, Openposition[rand].rotation);
               // Openposition.Remove(Openposition[rand]);
                int r = Random.Range(1, 3);
                
                switch (r)
                {
                    case 1:
                        if (_GreenNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        break;

                    case 2:
                        if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        break;

                    case 3:
                        if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Blue;
                            _BlueNpc--;
                        }
                        else if (_redNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Red;
                            _redNpc--;
                        }
                        else if (_BlueNpc != 0)
                        {
                            Npc.GetComponent<EnemyType>().color = EnemyType.enemyColor.Green;
                            _GreenNpc--;
                        }
                        break;
                }
                i--;
            } while (i >0);
        }
      


    }

    public void MapGenerator(int x, int y)
    {
        if (Map != null)
        {
            DestroyImmediate(Map);
        }
        Map = new GameObject();
        Map.AddComponent<MapInformation>();
        Map.name = "GameMap";
        
        for(int i=0;i< x;i++)
        {

            for(int j=0; j<y;j++)
            {
                int rand = Random.Range(0, MapCells.Count-1);
                GameObject newMapCell = (GameObject) Instantiate(MapCells[rand], new Vector3(i*(CellSize.x+Gritgab),j*(CellSize.y+Gritgab), 0), transform.rotation) ;
                newMapCell.name = "Cell " + i + "/" + j;
                if(newMapCell.GetComponent<MapCell>()==null)
                    newMapCell.AddComponent<MapCell>();
                newMapCell.GetComponent<MapCell>().GritPosition = new Vector2(i, j);
                newMapCell.transform.SetParent(Map.transform);
                Map.GetComponent<MapInformation>().MapCells.Add(newMapCell.GetComponent<MapCell>());
            }
           

        }
        int goal = Random.Range(0, Map.GetComponent<MapInformation>().MapCells.Count - 1);
        GameObject Goal = Instantiate(Goalprefab);
        GameObject zwisch = Map.GetComponent<MapInformation>().MapCells[goal].gameObject;
        Goal.transform.position = Map.GetComponent<MapInformation>().MapCells[goal].transform.position;
        Goal.name = "Goal";
        zwisch.name = "Huch";
       // zwisch.gameObject.SetActive(false);
        Map.GetComponent<MapInformation>().MapCells.RemoveAt(goal);
        DestroyImmediate(zwisch);
        Goal.transform.SetParent(Map.transform);

        BoardSetup();
         Respawn();
    }


    void BoardSetup()
    {
        
        GameObject boardHolder = new GameObject();
        boardHolder.name = "Boardholder";
       
        for (int x = -1; x < MapsizeX * CellSize.x + 1; x++)
        {
            //Loop along y axis, starting from -1 to place floor or outerwall tiles.
            for (int y = -1; y < MapsizeY*CellSize.y + 1; y++)
            {
                //Choose a random tile from our array of floor tile prefabs and prepare to instantiate it.
                //GameObject toInstantiate = Wallprefab;

                //Check if we current position is at board edge, if so choose a random outer wall prefab from our array of outer wall tiles.
                if (x == -1 || x == MapsizeX * CellSize.x || y == -1 || y == MapsizeY * CellSize.y)
                {
                    GameObject instance = Instantiate(Wallprefab, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                    //Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
                    instance.transform.SetParent(boardHolder.transform);
                    boardHolder.transform.SetParent(Map.transform);
                }
            }
        }
    }
}
