﻿using UnityEngine;
using System.Collections.Generic;

public class MapCell : MonoBehaviour {

    public Vector2 GritPosition;
	public bool[,] blocked=new bool[5,5];
	public List<Transform> freeCells=new List<Transform>();

	void Start(){
       // Freeposition();

    }
    public void Freeposition()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject child = transform.GetChild(i).gameObject;
            BoxCollider2D collider = child.gameObject.GetComponent<BoxCollider2D>();
            if (collider != null)
            {
                for (int x = 0; x < collider.size.x; x++)
                {
                    for (int y = 0; y < collider.size.y; y++)
                    {
                        blocked[x + (int)child.transform.localPosition.x, y + (int)child.transform.localPosition.y] = true;
                    }
                }
            }
        }

        for (int j = 0; j < transform.childCount; j++)
        {
            if (!Blocked((Vector2)transform.GetChild(j).localPosition))
            {
                freeCells.Add(transform.GetChild(j));
            }
        }
    }
	bool Blocked(Vector2 pos){
		return blocked[(int)pos.x,(int)pos.y];
	}
}
