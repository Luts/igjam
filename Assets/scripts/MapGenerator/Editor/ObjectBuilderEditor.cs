﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(mapgenerator))]
public class ObjectBuilderEditor :Editor  {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        mapgenerator myScript = (mapgenerator)target;
        if (GUILayout.Button("Build Map"))
        {
            myScript.MapGenerator(myScript.MapsizeX,myScript.MapsizeY);
        
        }
    }
}
