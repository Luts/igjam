﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

    private Collider2D[] check;
    private mapgenerator map;

    public Vector2 target;
    public float speedVal;

    void Start()
    {
        target = transform.position;
        //map = transform.Find("MapGenerator").GetComponent<mapgenerator>();
    }

    void Update () {
        check = Physics2D.OverlapCircleAll(target, GetComponent<Collider2D>().bounds.extents.magnitude);
        int checkCount = 0;
        speedVal = 0;

        /*foreach (Collider2D cell in check)
        {
            checkCount++;
            switch (cell.name)
            {
                case "grass":
                    speedVal *= 0.6f;
                    break;

                case "sidewalk":
                    speedVal *= 1.2f;
                    break;

                case "street":
                    speedVal *= 1.3f;
                    break;

                default:
                    break;
            }

        }*/

        for (int i=0; i < check.Length; i++)
        {
            //int checkCount = 0;
            //float speedVal = 0;

            if (check[i].ToString() == "street")
            {
                checkCount++;
                speedVal += 1.5f;
            }

            if (check[i].name == "sidewalk")
            {
                checkCount++;
                speedVal += 1.2f;
            }

            if (check[i].name == "grass")
            {
                checkCount++;
                speedVal += 0.8f;
            }
            Debug.Log(check[i]);
        }
    }
}
