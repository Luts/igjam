﻿using UnityEngine;
using System.Collections.Generic;

public class LevelProgression : MonoBehaviour
{
	public mapgenerator mapgen;
	public float percentNeeded=0.3f;
	public int level=1;
	public float kills=0;
	public float people;
	public List<LevelInfo> levelInfos = new List<LevelInfo>();

	public UnityEngine.UI.Text levelText;
	public UnityEngine.UI.Button button;
	public UnityEngine.UI.Image blackScreen;
	public UnityEngine.UI.Text pun;
	public UnityEngine.UI.Text timeText;
	public UnityEngine.UI.Text killsText;

	public AudioClip laugh1,laugh2;

	public float time=0;

	public bool load=false;
	public bool started=false;

	// Use this for initialization
	void Start ()
    {
		BuildLevel();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(load){
			load=false;
			mapgen.MapGenerator(mapgen.MapsizeX,mapgen.MapsizeY);
			button.gameObject.SetActive(true);
		}


		killsText.text="Voters: "+Mathf.Floor(kills)+" / "+Mathf.Ceil(people*percentNeeded);

		time+=Time.deltaTime;
		timeText.text="Time: "+Mathf.Floor(time);

		if (kills>=Mathf.Ceil(people*percentNeeded))
        { //when 80% are killed u win
			kills=0;
			time=0;
			level++;

			if (Random.value>0.5f)
				GetComponent<AudioSource>().clip=laugh1;
			else
				GetComponent<AudioSource>().clip=laugh2;
			GetComponent<AudioSource>().Play();

            if (level > levelInfos.Count)
            { //game over
                UnityEngine.SceneManagement.SceneManager.LoadScene("end");
            }
            else
            {// next level
                BuildLevel();
            }
		}

		if (started){
			levelText.color=new Color(levelText.color.r,levelText.color.g,levelText.color.b,levelText.color.a-Time.deltaTime*0.5f);
			blackScreen.color=new Color(blackScreen.color.r,blackScreen.color.g,blackScreen.color.b,blackScreen.color.a-Time.deltaTime*0.5f);
		}if (levelText.color.a <= 0)
        {
			levelText.gameObject.SetActive(false);
			blackScreen.gameObject.SetActive(false);
        }
	}

	public void BuildLevel()
    {	//build the level
		//kill remaining enemies
		for(int i=0; i<KI.enemies.Count; i++)
        {
            if (KI.enemies[i])
            {
                Destroy(KI.enemies[i].gameObject);
            }
		}
		KI.enemies.Clear();

		//show level text
		levelText.gameObject.SetActive(true);
		if (level<levelInfos.Count)
			levelText.text="Level "+level+" of "+(levelInfos.Count);
		else	
			levelText.text="Last Level!!!1";

		levelText.color=new Color(levelText.color.r,levelText.color.g,levelText.color.b,1);

		//pun
		pun.text=GetComponent<PunList>().badpuns[Random.Range(0,GetComponent<PunList>().badpuns.Count-1)].text;


		blackScreen.gameObject.SetActive(true);
		blackScreen.color=new Color(blackScreen.color.r,blackScreen.color.g,blackScreen.color.b,1);
		pun.gameObject.SetActive(true);

		mapgen.MapsizeX=(int)levelInfos[level-1].mapSize.x;
		mapgen.MapsizeY=(int)levelInfos[level-1].mapSize.y;
		mapgen.maxBlueNpc=levelInfos[level-1].blues;
		mapgen.maxRedNpc=levelInfos[level-1].reds;
		mapgen.maxGreenNpc=levelInfos[level-1].greens;

		people=mapgen.maxBlueNpc+mapgen.maxGreenNpc+mapgen.maxRedNpc;

		load=true;
		started=false;

	}

	public void StartLevel(){
		button.gameObject.SetActive(false);
		pun.gameObject.SetActive(false);
		started=true;
	}

}
