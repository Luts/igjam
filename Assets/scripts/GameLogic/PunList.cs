﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PunList : MonoBehaviour
{

    public List<BadPun> badpuns = new List<BadPun>();

    // Use this for initialization
    void Awake ()
    {
        badpuns.Add(new BadPun("Prime(Minister) Directive", "A welsh politician asked the government for information about UFO sightings and if it might fund UFO research. Officials wrote back, “jang vIDa je due luq … ach ghotvam’e’ QI’yaH devolve qaS.” Which means, “The minister will reply in due course. However, this is a non-devolved matter,” in Klingon.", "Source: bbc.com"));
        badpuns.Add(new BadPun("A Tough Question", "If con is the opposite of pro, then isn’t Congress the opposite of progress?", "Source: Jon Stewart"));
        badpuns.Add(new BadPun("The Best of The Onion Magazine Covers #1", "“I Thought He Was Going to Kill Me”: One Woman’s Harrowing Misunderstanding of How Haircuts Work", "Source: The Onion Magazine: The Iconic Covers That Transformed an Undeserving World (Little, Brown)"));
        badpuns.Add(new BadPun("The Best of The Onion Magazine Covers #2", "The 100 Worst Senators", "Source: The Onion Magazine: The Iconic Covers That Transformed an Undeserving World (Little, Brown)"));
        badpuns.Add(new BadPun("The Best of The Onion Magazine Covers #3", "The World’s 10 Most Powerful Women: We Make Them Discuss Fashion and Lindsay Lohan", "Source: The Onion Magazine: The Iconic Covers That Transformed an Undeserving World (Little, Brown)"));
        badpuns.Add(new BadPun("Jimmy Fallon on ISIS", "The Pentagon announced that its fight against ISIS will be called Operation Inherent Resolve. They came up with that name using Operation Random Thesaurus.", "Source: Jimmy Fallon"));
        badpuns.Add(new BadPun("The Star of Cake Boss Was Arrested…", "The star of Cake Boss was arrested for DWI. Police interrogated him for 30 minutes at 350 degrees.", "Source: Joe Toplyn"));
        badpuns.Add(new BadPun("Parenting Fads According #1", "Couples are waiting to announce their pregnancy until after their child has graduated college and become a partner in a successful law firm.", "Source: The Onion Magazine: The Iconic Covers That Transformed an Undeserving World (Little, Brown)"));
        badpuns.Add(new BadPun("Parenting Fads According #2", "Parents are choosing not to learn the gender of their obstetrician.", "Source: The Onion Magazine: The Iconic Covers That Transformed an Undeserving World (Little, Brown)"));
        badpuns.Add(new BadPun("Parenting Fads According #3", "As part of the new Infinity Womb trend, women are using a wide range of Lamaze, strength-training, and yoga techniques to forcefully prevent their children from ever leaving their wombs, forever protecting them from the harsh realities of the world.", "Source: The Onion Magazine: The Iconic Covers That Transformed an Undeserving World (Little, Brown)"));
        badpuns.Add(new BadPun("Airport Insecurity #1", "The head of the TSA resigned after about four years on the job. Here’s how much of America heard the news.", "Source: Conan O’Brien, on Conan"));
        badpuns.Add(new BadPun("Airport Insecurity #2", "[John Pistole retired today.] His employees toasted him with less than three ounces of champagne. Then they gave him a gold watch, and he had to take it off and put it in a bin.", "Source: Conan O’Brien, on Conan"));
        badpuns.Add(new BadPun("Airport Insecurity #3", "He actually stepped down a while ago, but he’s been going through security for three and a half years.", "Source: Jimmy Fallon, on The Tonight Show"));
        badpuns.Add(new BadPun("Notable Never-isms #1", "Never try to tell everything you know. It may take too short a time.", "Source: Norman Ford"));
        badpuns.Add(new BadPun("Notable Never-isms #2", "Never trust a man when he’s in love, drunk, or running for office.", "Source: Shirley Maclaine"));
        badpuns.Add(new BadPun("Notable Never-isms #3", "Never board a commercial aircraft if the pilot is wearing a tank top.", "Source: Dave Barry"));
        badpuns.Add(new BadPun("Notable Never-isms #4", "Never be in a hurry to terminate a marriage. You may need this person to finish a sentence.", "Source: Erma Bombeck"));
        badpuns.Add(new BadPun("Notable Never-isms #5", "Never argue with a doctor! He has inside information.", "Source: Bob Elliott and Ray Goulding"));
        badpuns.Add(new BadPun("Notable Never-isms #6", "Never keep up with the Joneses. Drag them down to your level; it’s cheaper.", "Source: Quentin Crisp"));
        badpuns.Add(new BadPun("Casting a Spell", "Can’t believe the National Spelling Bee ended in a tye.", "Source: MattGoldich"));
        badpuns.Add(new BadPun("Common Ground", "The Olympics remind us that no matter what country we may be from, we all look dumb using an iPad as a camera.", "Source: DCpierson"));
        badpuns.Add(new BadPun("…And Your Little Blog, Too!", "What if the whole ice-bucket challenge is just a long game to bring down the Wicked Witch of the West?", "Source: Apocalypsehow"));
        badpuns.Add(new BadPun("Say it With Your Pants", "Boy, what a bad guy that guy is, that Vladimir Putin. Obama is really getting tough with him. Now he’s wearing a much more aggressive shade of beige.", "Source: David Letterman"));
        badpuns.Add(new BadPun("You Get What You Pay For", "Dollar Tree bought Family Dollar for about $8 billion. It would have been $10 billion, but Family Dollar was dented.", "Source: JoeToplyn"));
        badpuns.Add(new BadPun("Cut and Run", "The one thing I’ve learned from the World Cup is that Europe still hasn’t mastered the haircut.", "Source: Bazecraze"));
        badpuns.Add(new BadPun("Anonymous", "I asked a Chinese girl for her number. She said, 'Sex!Sex!Sex!Free sex tonight!' I said, 'Wow!' Then her friend said, 'She means 666 - 3629.'", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #1", "Yo momma's so fat, I took a picture of her last Christmas and it's still printing.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #2", "Yo momma's so fat when she got on the scale it said, 'I need your weight not your phone number.'", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #3", "Yo momma's so fat, that when she fell, no one was laughing but the ground was cracking up.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #4", "Yo momma's so fat when she sat on WalMart, she lowered the prices!", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #5", "Yo momma's so stupid when an intruder broke into her house, she ran downstairs, dialed 9-1-1 on the microwave, and couldn't find the 'CALL' button.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #6", "Yo momma's so fat and old when God said, 'Let there be light,' he asked your mother to move out of the way.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #7", "Yo momma's so ugly when she tried to join an ugly contest they said, 'Sorry, no professionals.'", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("TrentHinger21", "If con is the opposite of pro, then is Congress the opposite of progress?", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Anonymous", "Politicians and diapers have one thing in common: they should both be changed regularly… and for the same reason.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("TheLaughFa...", "Q: How many politicians does it take to change a light bulb? \n A: Two: one to change it and another one to change it back again.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("NERO", "Did you hear about Monica Lewinsky becoming a Republican? The Democrats left a bad taste in her mouth.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Delilah Ro...", "The words election and erection are spelled similarly. They both have the same meaning too: a dick rising to power.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Shedpal", "The recession is getting so bad, the bank sent me a new type of credit card. It was pre-declined.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Anonymous", "Q: What is Rodney King's least favorite band? \n A: The Police.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("M@mm1", "Q: What's the difference between 9/11 and a cow? \n A: You can't milk a cow for over 10 years.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Anonymous", "What do you get when you cross a corrupt lawyer with a crooked politician? Chelsea Clinton.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Oblama", "New Jersey Governor Chris Christie has said that he may run for President, but analysts predict it is much more likely that he will walk.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Mark My W...", "Bill Clinton is no longer playing the saxophone. He is now playing the whore-Monica.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Anonymous", "It was so cold today, a Democrat had his hands in his own pockets!", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Hardball", "When asked if they would have sex with Bill Clinton, 86% of women in D.C. said, 'Not again.'", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("NERO", "Did you hear about Monica Lewinsky becoming a Republican? The Democrats left a bad taste in her mouth.", "Source: Internet"));
        badpuns.Add(new BadPun("Mark My Words", "Q: What did Saddam Hussein and Little Miss Muffit have in common? \n A: They both had curds (Kurds) in their way (whey).", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("Yo momma #8", "Yo momma's classless she could be a Marxist utopia.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("msantopoalo", "What's black, white, red, blue, green, yellow, purple, orange, and super easy for anyone to do? Michelle McGee, Jesse James' mistress.", "Source: www.laughfactory.com"));
        badpuns.Add(new BadPun("TheLaughFa...", "Q: What did Osama Bin Laden's ghost say to Mitt Romney? \n A: 'Don't be sad, Obama's foreign policy killed me too'", "Source: www.laughfactory.com"));

    }
    
}
