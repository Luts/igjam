﻿using UnityEngine;
using System.Collections;


#if UNITY_EDITOR
using UnityEditor;
#endif 
[System.Serializable]
public class LevelInfo
{
	public int reds,blues,greens;
	public Vector2 mapSize;
}
