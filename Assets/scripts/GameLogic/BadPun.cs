﻿
using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class BadPun
{
    public string title, text, source;

    public BadPun(string newtitle, string newtext, string newsource)
    {
        title = newtitle;
        text = newtext;
        source = newsource;
    }
}