﻿using UnityEngine;
using System.Collections;

public class EndScreenAnim : MonoBehaviour {
	public Sprite frame1,frame2;

	public float currentFrame=0;
	public float animSpeed=1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		currentFrame+=Time.deltaTime*animSpeed;

		if ((int)currentFrame%2==0)
			GetComponent<UnityEngine.UI.Image>().sprite=frame1;	
		else
			GetComponent<UnityEngine.UI.Image>().sprite=frame2;	

	}
}
