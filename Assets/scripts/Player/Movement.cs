﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public Masks maskSprite;
    public SpriteRenderer spR;
    public Sprite upwalkSprite_00;
    public Sprite downwalkSprite_00;
    public Sprite leftwalkSprite_00;
    public Sprite upwalkSprite_01;
    public Sprite downwalkSprite_01;
    public Sprite leftwalkSprite_01;

    private int step = 0;

    public float speed = 6.0f;
    private bool onMove;
    private Vector2 targetPosition;

    const int LEFT_MOUSE_BUTTON = 0;

    private float compareX;
    private float compareY;
    private float compareXAbs;
    private float compareYAbs;
    private bool wallColl = false;
    void Start()
    {
        spR = GetComponent<SpriteRenderer>();
        targetPosition = transform.position;
        onMove = false;
    }

    void Update()
    {
        if (wallColl == false)
        {
            compareX = transform.position.x - targetPosition.x;
            compareY = transform.position.y - targetPosition.y;
            compareXAbs = Mathf.Abs(compareX);
            compareYAbs = Mathf.Abs(compareY);
        }
        if (Input.touchSupported)
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    setTargetPosition();
                }
            }
        }
        else
        {
            if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
            {
                setTargetPosition();
            }
        }
        if (onMove == true)
        { 
            movePlayer();
        }
    }

    void setTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
	    	onMove = true;

			targetPosition = ray.GetPoint(100);
		} 
    }

    void movePlayer()
    {
        transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

            if (compareXAbs <= compareYAbs && Mathf.Sign(compareY) == -1)
            {
                spinUp();
                maskSprite.spinUp();
            }
            if (compareXAbs > compareYAbs && Mathf.Sign(compareX) == 1)
            {
                spinLeft();
                maskSprite.spinLeft();
            }
            if (compareXAbs > compareYAbs && Mathf.Sign(compareX) == -1)
            {
                spinRight();
                maskSprite.spinRight();
            }
            if (compareXAbs <= compareYAbs && Mathf.Sign(compareY) == 1)
            {
                spinDown();
                maskSprite.spinDown();
            }

        if (transform.position.x == targetPosition.x && transform.position.y == targetPosition.y)
        {
            onMove = false;
        }
    }

    void spinUp()
    {
        transform.localScale = new Vector2(1, 1);
        step++;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = upwalkSprite_00;
                    break;
                }
            case 6:
                {
                    spR.sprite = upwalkSprite_01;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spinDown()
    {
        transform.localScale = new Vector2(1, 1);
        step++;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = downwalkSprite_01;
                    break;
                }
            case 6:
                {
                    spR.sprite = downwalkSprite_00;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spinLeft()
    {
        transform.localScale = new Vector2(-1, 1);
        step++;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = leftwalkSprite_01;
                    break;
                }
            case 6:
                {
                    spR.sprite = leftwalkSprite_00;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spinRight()
    {
        transform.localScale = new Vector2(1, 1);
        step++;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = leftwalkSprite_01;
                    break;
                }
            case 6:
                {
                    spR.sprite = leftwalkSprite_00;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            onMove = false;
            wallColl = true;
            Vector2 normal = collision.contacts[0].normal;
            transform.position = (Vector2)transform.position + normal * 0.001f;
            targetPosition = transform.position;
        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        wallColl = false;
    }

}