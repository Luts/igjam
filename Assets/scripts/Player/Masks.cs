﻿using UnityEngine;
using System.Collections;

public class Masks : MonoBehaviour
{
    public enum color {Red, Green, Blue};
    public color col = color.Blue;

    public SpriteRenderer spR;
    public Sprite maskRedSprite_00;
    public Sprite maskGreenSprite_00;
    public Sprite maskBlueSprite_00;
    public Sprite maskRedSprite_01;
    public Sprite maskGreenSprite_01;
    public Sprite maskBlueSprite_01;

    public float speed = 6.0f;
    private Vector2 varSpinUp;
    private Vector2 varSpinDown;
    private Vector2 varSpinLeft;
    private Vector2 varSpinRight;
    
    const int LEFT_MOUSE_BUTTON = 0;

    private float compareX;
    private float compareY;
    private float compareXAbs;
    private float compareYAbs;

    void Start()
    {
        spR = GetComponent<SpriteRenderer>();
        switch (col)
        {
            case color.Blue:
                {
                    spR.sprite = maskBlueSprite_00;
                    break;
                }
            case color.Red:
                {
                    spR.sprite = maskRedSprite_00;
                    break;
                }
            case color.Green:
                {
                    spR.sprite = maskGreenSprite_00;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void Update()
    {
        varSpinDown = new Vector2(0f, 0.2f); 
        varSpinRight = new Vector2(0.1f, 0.2f);
        varSpinLeft = new Vector2(0.1f, 0.2f);
        varSpinUp = new Vector2(0f, 0.3f);
        askColor();
    }

    public void spinUp()
    {
        spR.enabled = false;
        transform.localPosition = varSpinUp;
    }

    public void spinDown()
    {
        spR.enabled = true;
        switch (col)
        {
            case color.Blue:
                {
                    spR.sprite = maskBlueSprite_00;
                    break;
                }
            case color.Red:
                {
                    spR.sprite = maskRedSprite_00;
                    break;
                }
            case color.Green:
                {
                    spR.sprite = maskGreenSprite_00;
                    break;
                }
            default:
                {
                    break;
                }
        }
        transform.localPosition = varSpinDown;
    }

    public void spinLeft()
    {
        spR.enabled = true;
        switch (col)
        {
            case color.Blue:
                {
                    spR.sprite = maskBlueSprite_01;
                    break;
                }
            case color.Red:
                {
                    spR.sprite = maskRedSprite_01;
                    break;
                }
            case color.Green:
                {
                    spR.sprite = maskGreenSprite_01;
                    break;
                }
            default:
                {
                    break;
                }
        }
        transform.localPosition = varSpinLeft;
    }

    public void spinRight()
    {
        spR.enabled = true;
        switch (col)
        {
            case color.Blue:
                {
                    spR.sprite = maskBlueSprite_01;
                    break;
                }
            case color.Red:
                {
                    spR.sprite = maskRedSprite_01;
                    break;
                }
            case color.Green:
                {
                    spR.sprite = maskGreenSprite_01;
                    break;
                }
            default:
                {
                    break;
                }
        }
        transform.localPosition = varSpinRight;
    }
    void askColor()
    {
        switch (col)
        {
            case color.Blue:
                {
                    if (spR.sprite == maskGreenSprite_01 || spR.sprite == maskBlueSprite_01 || spR.sprite == maskRedSprite_01)
                    {
                        spR.sprite = maskBlueSprite_01;
                    }
                    else
                    {
                        spR.sprite = maskBlueSprite_00;
                    }
                    break;
                }
            case color.Red:
                {
                    if (spR.sprite == maskGreenSprite_01 || spR.sprite == maskBlueSprite_01 || spR.sprite == maskRedSprite_01)
                    {
                        spR.sprite = maskRedSprite_01;
                    }
                    else
                    {
                        spR.sprite = maskRedSprite_00;
                    }
                    break;
                }
            case color.Green:
                {
                    if (spR.sprite == maskGreenSprite_01 || spR.sprite == maskBlueSprite_01 || spR.sprite == maskRedSprite_01)
                    {
                        spR.sprite = maskGreenSprite_01;
                    }
                    else
                    {
                        spR.sprite = maskGreenSprite_00;
                    }
                    break;
                }
            default:
                {
                    break;
                }
        }
    }
}