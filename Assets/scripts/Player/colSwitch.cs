﻿using UnityEngine;
using System.Collections;

public class colSwitch : MonoBehaviour
{
	public Masks maskCol;
	public UnityEngine.UI.Image image;
	public Sprite red,blue,green;

    public void switchCol()
    {
		GetComponent<AudioSource>().Play();

        switch (maskCol.col)
        {
            case Masks.color.Blue:
                    maskCol.col = Masks.color.Red;
					image.sprite=green;
                    break;
            case Masks.color.Green:
                    maskCol.col = Masks.color.Blue;
					image.sprite=red;
					break;
            case Masks.color.Red:
                    maskCol.col = Masks.color.Green;
					image.sprite=blue;
					break;
        }
    }
}
