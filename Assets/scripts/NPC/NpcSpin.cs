﻿using UnityEngine;
using System.Collections;

public class NpcSpin : MonoBehaviour
{
    public KI npcGoal;
    public SpriteRenderer spR;
    public Sprite upwalkSprite_00;
    public Sprite downwalkSprite_00;
    public Sprite rightwalkSprite_00;
    public Sprite upwalkSprite_01;
    public Sprite downwalkSprite_01;
    public Sprite rightwalkSprite_01;

	public Sprite headFront,headBack;
	public SpriteRenderer head;

    public int step = 0;

    private float compareX;
    private float compareY;
    private float compareXAbs;
    private float compareYAbs;
    private Vector2 targetPosition;

    void Start()
    {
        npcGoal = GetComponent<KI>();
        spR = GetComponent<SpriteRenderer>();
        targetPosition = transform.position;
    }

    void Update()
    {
        targetPosition = npcGoal.goal;
        compareX = transform.position.x - targetPosition.x;
        compareY = transform.position.y - targetPosition.y;
        compareXAbs = Mathf.Abs(compareX);
        compareYAbs = Mathf.Abs(compareY);
        step++;

        spin();
    }

    void spin()
    {
        if (compareXAbs <= compareYAbs && Mathf.Sign(compareY) == -1)
        {
            spinUp();
        }
        if (compareXAbs > compareYAbs && Mathf.Sign(compareX) == 1)
        {
            spinLeft();
        }
        if (compareXAbs > compareYAbs && Mathf.Sign(compareX) == -1)
        {
            spinRight();
        }
        if (compareXAbs <= compareYAbs && Mathf.Sign(compareY) == 1)
        {
            spinDown();
        }
    }


    void spinUp()
    {
        transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), Mathf.Abs(transform.localScale.y));
		head.sprite=headBack;

        switch (step)
        {
            case 1:
                {
                    spR.sprite = upwalkSprite_00;
                    break;
                }
            case 6:
                {
                    spR.sprite = upwalkSprite_01;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spinDown()
    {
        transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), Mathf.Abs(transform.localScale.y));

		head.sprite=headFront;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = downwalkSprite_01;
                    break;
                }
            case 6:
                {
                    spR.sprite = downwalkSprite_00;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spinLeft()
    {
		transform.localScale = new Vector2(-Mathf.Abs(transform.localScale.x), Mathf.Abs(transform.localScale.y));
		head.sprite=headFront;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = rightwalkSprite_01;
                    break;
                }
            case 6:
                {
                    spR.sprite = rightwalkSprite_00;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    void spinRight()
    {
		transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x), Mathf.Abs(transform.localScale.y));
		head.sprite=headFront;
        switch (step)
        {
            case 1:
                {
                    spR.sprite = rightwalkSprite_01;
                    break;
                }
            case 6:
                {
                    spR.sprite = rightwalkSprite_00;
                    break;
                }
            case 10:
                {
                    step = 0;
                    break;
                }
            default:
                {
                    break;
                }
        }
    }
}