﻿using UnityEngine;
using System.Collections;

public class DieAble : MonoBehaviour {
	public bool isDying=false;
	public float dragSpeed=1;
	public float fallSpeed=0.99f;
	public Transform hole;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isDying){
			transform.localScale=transform.localScale*fallSpeed;
			transform.Translate((hole.position-transform.position)*dragSpeed*Time.deltaTime);

			if (transform.localScale.magnitude<=0.1){
				Ref.instance.levelProgression.kills+=1;
				Destroy(gameObject);
			}
		}

	}
		
	void OnTriggerEnter2D(Collider2D c){
		if (c.GetComponent<Goal>()){
			isDying=true;
			hole=c.transform;
            GetComponent<EnemyType>().movement = EnemyType.enemyMovement.Die;
            GetComponent<KI>().resetSound();
            if (GetComponent<playsound>().Died.Length != 0)
            {
                int rand = Random.Range(0, 2);
                GetComponent<AudioSource>().PlayOneShot(GetComponent<playsound>().Died[rand], 1);
            }
        }	}
}
