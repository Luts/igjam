﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KI : MonoBehaviour
{
	public static List<KI> enemies=new List<KI>();

	public Transform trigger;
	public EnemyType enemyType;
	public SpriteRenderer reactObject;

	public float reactAlpha=0;

	public float currentSpeed=0.1f;
	public float idleSpeed=0.5f;
	public float catchSpeed=0.5f;
	public float fleeSpeed=0.5f;


    public float comfortZone;
	public float panicZone;

	public float movementRange;
	public float idleRange;
	public float fleeRange=10;
	public float catchRange=10;

	public float newGoalCounter;
	public float newGoalCounterMin=1;
	public float newGoalCounterMax=2;

	public float kiUpdateCounter=0;
	public float kiUpdateCounterMin=0.5f;
	public float kiUpdateCounterMax=1.5f;

	public float detectRange=5;
	public Vector2 direction;
    public Vector2 goal;
    public Vector2 startOrigin =new Vector2() ;
    public Vector2 dir;


    public Transform player;

    void Awake()
    {
		enemies.Add(this);

        if (GetComponent<EnemyType>() == null)
        {
            gameObject.AddComponent<EnemyType>();
        }
		enemyType=GetComponent<EnemyType>();

        startOrigin = transform.position;



		RandomGoal();
    }

	void Start(){
		player=Ref.instance.player;
	}

    void Update()
    {
		if (!Ref.instance.levelProgression.started)
			return;

		kiUpdateCounter-=Time.deltaTime;

		if (kiUpdateCounter<=0){
			kiUpdateCounter=Random.Range(kiUpdateCounterMin,kiUpdateCounterMax);
			switch (GetComponent<EnemyType>().movement)
	        {
	            case EnemyType.enemyMovement.Idle:
					movementRange=idleRange;
	                Idle();
	                break;

	            case EnemyType.enemyMovement.Catch:
	                Catch();
	                break;

	            case EnemyType.enemyMovement.Flee:
	                Flee();
	                break;

	            default:
	                break;
	        }
		}

		Walk();

		reactAlpha-=Time.deltaTime;
		reactObject.color=new Color(reactObject.color.r,reactObject.color.g,reactObject.color.b,reactAlpha);

    }

	void RandomGoal(){
		goal = startOrigin + Random.insideUnitCircle * idleRange;
	}

    void Idle()
    {
		currentSpeed=idleSpeed;

		//Wenn der spieler gesehen wird,
		if (SeePlayer(detectRange)){
			switch(GetComponent<EnemyType>().behaviour){ //änder den state
			case EnemyType.Behavior.Catch:
				enemyType.movement=EnemyType.enemyMovement.Catch;
                    setHappySound();
				break;
			case EnemyType.Behavior.Flee:
				enemyType.movement=EnemyType.enemyMovement.Flee;
                    setAngrySound();
				break;
			case EnemyType.Behavior.Ignore:
				break;
			}
		}
    }
		

    void setHappySound()
    {
        if(GetComponent<playsound>().Happy.Length!=0)
        {
            int rand = Random.Range(0, 2);
            GetComponent<AudioSource>().clip = GetComponent<playsound>().Happy[rand];
            GetComponent<AudioSource>().Play();
            GetComponent<AudioSource>().loop = true;
           

        }
        else { resetSound(); }
    }
    void setAngrySound()
    {
        if (GetComponent<playsound>().Fear.Length != 0)
        {
            int rand = Random.Range(0, 2);
            GetComponent<AudioSource>().clip = GetComponent<playsound>().Fear[rand];
            GetComponent<AudioSource>().Play();
            GetComponent<AudioSource>().loop = true;
            ;

        }
        else { resetSound(); }
    }

  public  void resetSound()
    {
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = null;
        GetComponent<AudioSource>().loop = false;

    }
    void Catch()
    {
		goal=(Vector2)player.position;
		currentSpeed = catchSpeed;

		//Wenn der spieler nicht gesehen wird, lauf idle herum
		if (!SeePlayer(catchRange)){
			startOrigin=transform.position;
			enemyType.movement=EnemyType.enemyMovement.Idle;
            resetSound();
		}
		if (Vector2.Distance((Vector2)player.position,(Vector2)transform.position)<comfortZone){
			startOrigin=transform.position;
			RandomGoal();
		}
        if (enemyType.behaviour != EnemyType.Behavior.Catch)
        {
            enemyType.movement = EnemyType.enemyMovement.Idle;
            resetSound();
        }


		reactAlpha=1;
		reactObject.sprite=Ref.instance.reactionHeart;
	}

    void Walk()//Zum derzeitigen ziel laufen
	{
		//CollisionCheck((Vector2)transform.position);

		direction = (goal-(Vector2)transform.position).normalized;
		trigger.transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);

		transform.Translate(direction*currentSpeed*Time.deltaTime);
		//Wenn das ziel erreicht wurde, such ein neues ziel
		if (Vector2.Distance(transform.position,goal) < comfortZone)
			RandomGoal();
		
		if (newGoalCounter<=0){
			RandomGoal();
			newGoalCounter=Random.Range(newGoalCounterMin,newGoalCounterMax);
		}
		newGoalCounter-=Time.deltaTime;
    }

    void Flee()
    {
		//Renn in die entgegengesetze richtung wie der player ist
		goal=(Vector2)(transform.position+(transform.position-player.position).normalized*fleeRange);
		currentSpeed = fleeSpeed;

		//Wenn der spieler nicht gesehen wird, lauf idle herum
		if (Vector2.Distance((Vector2)player.position,(Vector2)transform.position)>idleRange){
			startOrigin=transform.position;
			enemyType.movement=EnemyType.enemyMovement.Idle;
            resetSound();
		}
        if (enemyType.behaviour != EnemyType.Behavior.Flee)
        {
            enemyType.movement = EnemyType.enemyMovement.Idle;
            resetSound();
        }

		reactAlpha=1;
		reactObject.sprite=Ref.instance.reactionTear;
    }

	bool SeePlayer(float distance){
		if (Vector2.Distance((Vector2)transform.position,(Vector2)player.position)>distance){
			return false;
		}

		RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, player.position-transform.position, distance);

		for (int i = 0; i<hit.Length; i++)
		{
			if (hit[i].transform.tag == "Wall")
			{
				return false;
			}
		}
		return true;
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Wall")
		{
			Vector2 normal = collision.contacts[0].normal;
			transform.position = (Vector2)transform.position + normal * 0.001f;
			goal = transform.position;
		}
	}
}
