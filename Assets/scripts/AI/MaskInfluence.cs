﻿using UnityEngine;
using System.Collections;

public class MaskInfluence : MonoBehaviour {

    public Transform player;
    public Color myRed = new Color();
    public Color myBlue = new Color();
    public Color myGreen = new Color();

    void Start()
    {
        player = Ref.instance.player;

        switch (GetComponent<EnemyType>().color)
        {
                case EnemyType.enemyColor.Red:
                ColorUtility.TryParseHtmlString("#CD052AFF", out myRed);
                GetComponent<SpriteRenderer>().color = myRed;
                break;

            case EnemyType.enemyColor.Blue:
                ColorUtility.TryParseHtmlString("#01375FFF", out myBlue);
                GetComponent<SpriteRenderer>().color = myBlue;
                break;

            case EnemyType.enemyColor.Green:
                ColorUtility.TryParseHtmlString("#0A9221FF", out myGreen);
                GetComponent<SpriteRenderer>().color = myGreen;
                break;

            default:
                break;
        }
    }

    void Update()
    {
        switch (GetComponent<EnemyType>().color)
        {
            case EnemyType.enemyColor.Blue:
                switch (player.GetComponentInChildren<Masks>().col)
                {
                    case Masks.color.Blue:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Catch;
                        break;

                    case Masks.color.Green:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Flee;
                        break;

                    case Masks.color.Red:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Ignore;
                        break;

                    default:
                        break;
                }
                break;

            case EnemyType.enemyColor.Green:
                switch (player.GetComponentInChildren<Masks>().col)
                {
                    case Masks.color.Blue:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Ignore;
                        break;

                    case Masks.color.Green:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Catch;
                        break;

                    case Masks.color.Red:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Flee;
                        break;

                    default:
                        break;
                }
                break;

            case EnemyType.enemyColor.Red:
                switch (player.GetComponentInChildren<Masks>().col)
                {
                    case Masks.color.Blue:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Flee;
                        break;

                    case Masks.color.Green:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Ignore;
                        break;

                    case Masks.color.Red:
                        GetComponent<EnemyType>().behaviour = EnemyType.Behavior.Catch;
                        break;

                    default:
                        break;
                }
                break;

            default:
                break;
        }
    }
}
