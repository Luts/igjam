﻿using UnityEngine;
using System.Collections;

public class EnemyType : MonoBehaviour {

    public enemyColor color;
    public npcType type;
    public enemyMovement movement;
	public Behavior behaviour;

    public enum enemyColor
    {
        Red, Green, Blue
    }

    public enum npcType
    {
        Block, Wander
    }

    public enum enemyMovement
    {
        Idle, Catch, Flee, Walk,Die
    }

	public enum Behavior{
		Flee, Catch, Ignore
	}
}
